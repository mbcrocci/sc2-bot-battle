from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer

from mbcrocci import MbcrocciBot
from rodasq import RodasQBot


run_game(maps.get("Abyssal Reef LE"), [
    Bot(Race.Zerg, MbcrocciBot()),
    Bot(Race.Zerg, RodasQBot())
], realtime=True)