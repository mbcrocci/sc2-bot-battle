import sc2
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer
from sc2.constants import *

class MbcrocciBot(sc2.BotAI):
    last_hatch_made  = None
    hq = None

    async def on_step(self, iteration):
        hq = self.units(HATCHERY).first

        # Scout
        if iteration == 0:
            await self.do(
                self.workers.first.attack(self.enemy_start_locations[0])
            )

        await self.build_units()
        await self.build_structures()
        await self.expand()
        await self.queen_inject()
        await self.attack()

    async def build_units(self):
        larvae = self.units(LARVA)

        if self.supply_left < 3:
            if larvae.exists and self.can_afford(OVERLORD) and self.already_pending(OVERLORD):
                await self.do(larvae.random.train(OVERLORD))

        if self.can_afford(DRONE) and larvae.exists:
            await self.do(larvae.random.train(DRONE))

        if self.units(SPAWNINGPOOL).exists:
            if self.can_afford(ZERGLING) and larvae.exists:
                await self.do(larvae.random.train(ZERGLING))

        if self.units(SPAWNINGPOOL).ready.exists:
            if not self.units(QUEEN).exists and hatchery.is_ready and hatchery.noqueue:
                if self.can_afford(QUEEN):
                    await self.do(hq.train(QUEEN))

    async def build_structures(self):
        if not self.units(SPAWNINGPOOL).exists and not self.already_pending(SPAWNINGPOOL):
            if self.can_afford(SPAWNINGPOOL):
                await self.build(SPAWNINGPOOL, near=hq)

        if not self.units(EXTRACTOR).exists or self.already_pending(EXTRACTOR):
            if self.can_afford(EXTRACTOR):
                await self.build(EXTRACTOR, near=h)

        elif self.units(EXTRACTOR).exists:
            ex = self.units(EXTRACTOR).random
            drones = self.workers.closer_than(20, ex)
            if drones.exists:
                await self.do(drones.do(drones.random.gather(ex)))

    async def expand(self):
        if self.can_afford(HATCHERY) and self.is_good_time_to_expand():
            await self.expand_now()

    async def build_research(self):
        if self.vespene >= 100:
            sp = self.units(SPAWNINGPOOL).ready
            if sp.exists and self.minerals >= 100 and not self.already_pending(RESEARCH_ZERGLINGMETABOLICBOOST):
                await self.do(sp.first(RESEARCH_ZERGLINGMETABOLICBOOST))

    async def attack(self):
        if self.units(ZERGLING).amount > 10:
            for zerg in self.units(ZERGLING):
                await self.do(zerg.attack(self.select_target())

    async def queen_inject(self):
        for queen in self.units(QUEEN).idle:
            abilities = await self.get_available_abilities(queen)
            if AbilityId.EFFECT_INJECTLARVA in abilities:
                await self.do(queen(EFFECT_INJECTLARVA, hq))

    def is_good_time_to_expand(self):
        if last_hatch_made.assigned_harvesters.amount > 16:
            pass

    async def select_target(self):
        if self.known_enemy_structures.exists:
            return random.choice(self.known_enemy_structures).position

        await self.enemy_start_locations[0]


run_game(maps.get("Abyssal Reef LE"), [
    Bot(Race.Zerg, MbcrocciBot()),
    Computer(Race.Terran, Difficulty.Easy)
], realtime=True)
